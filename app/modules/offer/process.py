# Imports from python distribution
import datetime
import time
import sys, traceback

# External libraries
from lxml import etree
from smart_open import smart_open
import requests

# Intra-project imports
from .parser import parseSkuPrice
from ...lib.httprequest import post
from ...lib.s3utils import readFromS3
from ...lib.xmlutils import incrementalParse
from ...lib.awsutils import snsNotify
from ...lib.errorutils import handleException
from json import loads

def parseAndPopulate(fp, config, event, context):
    ctx = {
        'file':fp,
        'lambda': {
            'function' : 'ydv-feed-develop-offers' \
                if context.function_name == 'Fake' else context.function_name,
            'requestId': context.aws_request_id
        }
    }
    root = incrementalParse(readFromS3(fp))
    starttime = datetime.datetime.now()
    tracker = {'count':0, 'totalPriceSkus':0, 'badRecords':0}
    priceSkus = []
    try :
        print ('Info: ', ctx)
        s = requests.Session()
        batchSize = int(config.get('batchSize'))
        pauseAfter = int(config.get('pauseAfterBatch')) * batchSize
        for elem in root.findall('./Sku'):
            tracker['count'] += 1
            tracker['totalPriceSkus'] += 1
            priceSkus.append(parseSkuPrice(elem, config.get('TZ_OFFSET')))
            if tracker.get('count') == batchSize:
                if not sendPost(config, elem, priceSkus, s, tracker):
                    return 5000
        if tracker['count'] in range(1, batchSize):
            if not sendPost(config, None, priceSkus, s, tracker):
                    return 5000

        print ('Elapsed time ', datetime.datetime.now()-starttime)
        result = {
            'processedRecords': tracker.get('totalPriceSkus'),
            'badRecords': tracker.get('badRecords'),
            'finished': True
        }
        print ('Result:', result)
        snsNotify(config.get('SNS_NOTIFY_TOPIC'), ctx, result = result)
        return 0

    except Exception as ex:
        return handleException(ex, ctx, notificationTarget = config.get('SNS_NOTIFY_TOPIC'))

def sendPost(config, elem, priceSkus, s, tracker):
    status = 0
    attempt = 0
    while status is not 200 :
        attempt +=1
        print ("Posting ",  tracker.get('totalPriceSkus'), tracker.get('count'), attempt, sys.getsizeof(priceSkus))
        response = post(config, priceSkus, s)
        status = response.status_code
        body = response.content
        print ('Response :: ', status, body)
        if status in [401, 403]:
            return False

        if status == 200:
            tracker['badRecords'] = tracker.get('badRecords') + loads(body).get('badRecordsCount')
            tracker['count'] = 0
            del priceSkus [:]
            if elem is not None:
                cleanup(elem)
            attempt = 0
            break
        else:
            print ("Reattempt Posting ",  tracker.get('totalPriceSkus'), tracker.get('count'))
    return True

def cleanup (elem) :
    # cleanup
    elem.clear()
    # delete previous siblings (records)
    while elem.getprevious() is not None:
        #print 'hasPrev'
        del elem.getparent()[0]
