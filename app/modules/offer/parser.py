import json
import sys
from lxml import etree
from datetime import datetime

def parseSkuPrice(skuPriceNode, tzOffset):
    price = {
        'extra': {}
    }
    price['name'] = 'eb_styleoffer'
    for child in skuPriceNode:
        if child.tag == 'sku_id':
            price['itemId'] = child.text
        elif child.tag in ['effort']: # Ignore this
            continue
        elif child.tag == 'style':
            price['name'] = 'eb_'+ child.text
            price['extra']['style'] = child.text
        elif child.tag == 'price_types':
            price['offers'] = parseOffers(child.findall('./price_type'), price, tzOffset)
    return price

def parseOffers(offersNode, skuPrice, tzOffset):
    offers = []
    for offerNode in offersNode :
        countryCode = offerNode.findtext('CountryCode', default = 'US')
        offer = {
            'channel' : 'com' if countryCode == 'US' else countryCode.lower()
        }
        for child in offerNode : 
            if child.tag == 'PriceId':
                offer['code'] = child.text
            if child.tag == 'effort':
                offer['kind'] = child.text
            elif child.tag == 'StartDate':
                offer['startDate'] = datetime.strptime(child.text,\
                    '%Y-%m-%d-%H:%M').strftime('%Y-%m-%dT%H:%M:%S' + tzOffset)
            elif child.tag == 'EndDate':
                offer['endDate'] = datetime.strptime(child.text,\
                    '%Y-%m-%d-%H:%M').strftime('%Y-%m-%dT%H:%M:%S' + tzOffset)
            elif child.tag == 'Price':
                offer['price'] = {
                    'currency' : countryCode + 'D',
                    'amount': child.text
                }
        offers.append(offer)
    return offers


def lookForBoolean(value):
    if value is not None:
        if value.lower() == 'true':
            return True
        elif value.lower() == 'false':
            return False
    return value




