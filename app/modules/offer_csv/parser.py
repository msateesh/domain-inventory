from datetime import datetime
def parse(row, header, tzOffset) :
    poBuffer = {}
    Quantity = {}
    data = {
        'offers' :[]
    }
    offer = {}
    price = {}
    for key, value in zip(header, row):   
        if key == 'SKU_ID':
            data['itemId'] = value
        elif key =='DISPLAY_NAME':
            data['name'] = value
        elif key =='START_DATE':
            offer['startDate'] = parseDate(value, tzOffset)
        elif key =='END_DATE':
            offer['endDate'] = parseDate(value)
        elif key =='EFFORT':
            offer['kind'] = value
        elif key =='FEED_ID':
            offer['code'] = value
        elif key =='SALE_PRICE':
            price['amount'] = value
        elif key =='CLEARANCE_PRICE':
            price['amount'] = value
    
    if offer.get('kind').startswith("7"):
        price['currency'] = 'USD'
        offer['channel'] =  'com'
    elif offer.get('kind').startswith("8"):
        price['currency'] = 'CAD'
        offer['channel'] =  'ca'

    offer['price'] = price
    data['offers'].append(offer)
    return data

def parseDate(dateString, tzOffset) :
    parts = dateString.split(' ')
    # [:-3] is for converting nanoseconds in feed to micro
    d = parts[0]+ " "+ parts[1][:-3]+" "+ parts[2]
    # Since the feed uses 12 hour format, %I is used
    ret = datetime.strptime(d, '%d-%b-%y %I.%M.%S.%f %p')
    # Y2K is back
    if ret.year < 2000:
        badyear = ret.year
        ret = ret.replace(year = badyear+100)
        del badyear
    return ret.strftime('%Y-%m-%dT%H:%M:%S' + tzOffset)

