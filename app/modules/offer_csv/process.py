# Imports from python distribution
import time
import sys, traceback
import csv
import datetime

from smart_open import smart_open
from requests import Session

# Intra-project imports
from .parser import parse
from ...lib.httprequest import post
from ...lib.awsutils import snsNotify
from ...lib.errorutils import handleException
from json import loads

def process(fp, config):
    starttime = datetime.datetime.now()
    tracker = {'count':0, 'lineCount':0}
    rows = []
    header = {}
    batchSize = int(config.get('batchSize'))
    pauseAfter = int(config.get('pauseAfterBatch')) * batchSize
    s = Session()
    with smart_open(fp, 'r') as fh:
        for line in fh:
            line = line.strip()
            if line:
                tracker['lineCount'] += 1
            if tracker['lineCount'] == 1: # First row
                row = next(csv.reader([line]))
                if row:
                    header = row
            else:
                tracker['count'] += 1
                row = next(csv.reader([line]))  
                if row:
                    data = parse(row, header)
                    print (data)
                    rows.append(data)
            if tracker.get('count') == batchSize:
                sendPost(config, rows, s, tracker)
    # The below check takes care of the last total % batchsize items
    # Which are skipped by the loop
    if tracker.get('count') in range(1, batchSize):
        sendPost(config, rows, s, tracker)

def parseAndPopulate(fp, config, event, context):
    ctx = {
        'file':fp,
        'lambda': {
            'function' : 'ydv-feed-develop-offers_csv' \
                if context.function_name == 'Fake' else context.function_name,
            'requestId': context.aws_request_id
        }
    }
    try:
        print ('Info: ', ctx)
        starttime = datetime.datetime.now()
        tracker = {'count':0, 'lineCount':0}
        rows = []
        header = {}
        batchSize = int(config.get('batchSize'))
        pauseAfter = int(config.get('pauseAfterBatch')) * batchSize
        s = Session()
        with smart_open(fp, 'r') as fh:
            for line in fh:
                line = line.strip()
                if line:
                    tracker['lineCount'] += 1
                if tracker['lineCount'] == 1: # First row
                    row = next(csv.reader([line]))
                    if row:
                        header = row
                else:
                    tracker['count'] += 1
                    row = next(csv.reader([line]))  
                    if row:
                        data = parse(row, header, config.get('TZ_OFFSET'))
                        rows.append(data)
                if tracker.get('count') == batchSize:
                    if not sendPost(config, rows, s, tracker):
                        return 5000
        # The below check takes care of the last total % batchsize items
        # Which are skipped by the loop
        if tracker.get('count') in range(1, batchSize):
            if not sendPost(config, rows, s, tracker):
                return 5000
        result = {
            'processedRecords': tracker.get('lineCount') - 1,
            'badRecords': tracker.get('badRecords'),
            'finished': True
        }
        print ('Result:', result)
        snsNotify(config.get('SNS_NOTIFY_TOPIC'), ctx, result = result)
        return 0
    except Exception as ex:
        return handleException(ex, ctx, notificationTarget = config.get('SNS_NOTIFY_TOPIC'))

def sendPost(config, rows, s, tracker):
    status = 0
    attempt = 0
    while status not in range(200,499):
        attempt +=1
        print ("Posting ",  tracker.get('lineCount'), tracker.get('count'), attempt, sys.getsizeof(rows))
        response = post(config, rows, s)
        body = response.content
        print ('Response :: ', status, body)
        if status in [401, 403]:
            return False
        if status in range(200, 499) or attempt > 5:
            tracker['badRecords'] = tracker.get('badRecords') + loads(body).get('badRecordsCount')
            tracker['count'] = 0
            del rows [:]
            attempt = 0
            break
        else:
            print ("Reattempt Posting ",  tracker.get('lineCount'), tracker.get('count'))
    return True