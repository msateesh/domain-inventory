def getQuery(name, limit = 999999, offset = 0, kwargs = {}) :
    queries = {
        'itemsAggregation': [
        {
            '$match':{'attributes':{'$elemMatch':{'name':'isActive', 'value': True}}}
        },
        {
            '$match':{'attributes':{'$elemMatch':{'name':'isDisplay', 'value': True}}}
        },
        {
            '$project': {
                "_id": {
                    "$ifNull": ["$parent", "$_id"]
                },
                "parent": {
                    "$ifNull": ["$parent", False]
                },
                'createdAt':1,
                'data': "$$ROOT"
            }
        }, 
        {
            '$sort': {
                "_id": 1,
                "parent": -1,
                "createdAt": 1
            }
        },
        {
            '$group': {
                '_id': '$_id', 
                'skus': {
                    '$push':'$data'
                }
            }
        },
        {
            '$skip': offset
        },
        {
            '$limit': limit
        }],
        'offersAggregation': [{
            '$match': { 'itemId': { '$in': kwargs.get('skulist') } }
        },
        {
            '$project' : {
                'itemId': 1,
                'offers': {
                    '$filter': {
                        'input': '$offers',
                        'as': 'offers',
                        'cond': {
                            '$and': [
                                { '$lte': ['$$offers.startDate', kwargs.get('dDate')] },
                                { '$gte': ['$$offers.endDate', kwargs.get('dDate')] },
                                { '$ne': ['$$offers.price.amount', 0] },
                                { '$in': ['$$offers.kind', ['792','892','709','809']] }
                            ]
                        }
                    }
                }
            }
        }]
    }
    return queries.get(name)