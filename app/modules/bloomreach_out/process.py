from datetime import datetime
import smart_open
import asyncio, csv
from io import StringIO
from aiohttp import ClientSession
from pymongo import UpdateOne
from pymongo.errors import BulkWriteError
from pysftp import Connection

from ...lib.mongoutils import getConnection, getConnectionAsync
from ...lib.httprequest import httpPost
from ...lib.jsonutils import jsonFileToDict, dump, loads
from ...lib.errorutils import handleException
from ...lib.fileutils import compressGz, decompressGz, writeToFtp
from ...lib.xmlutils import writeToFile, createDocumentRoot, createElement, printDocument, incrementalParse
from ...lib.awsutils import snsNotify
from .parser import parseAndLoadItem, getXmlTagName
from .queries import getQuery

async def fetchAndLoad(config, event, context):
    ctx = {
        'lambda': {
            'function' : 'ydv-feed-develop-bloomreach-out-daily' \
                if context.function_name == 'Fake' else context.function_name,
            'requestId': context.aws_request_id
        }
    }
    try:
        tracker = {
            'startTime' : datetime.now(),
            'complete' : True,
            'count' : 0
        }
        limit = int(config.get('LIMIT'))
        offset = int(config.get('OFFSET'))
        if event.get('eventName') == config.get('MANUAL_TEST_EVENT'):
            limit = event.get('query').get('limit')
            offset = event.get('query').get('offset')
        doc = createDocumentRoot('feed')
        global httpSession
        async with ClientSession() as session:
            httpSession = session
            data = await accumulate(doc, limit, offset, tracker, config, context)
        print ("Done accumlating and parsing, writing to file now",\
            tracker.get('count'), datetime.now() - tracker.get('startTime'))  
        ftpFile = config.get('OUT_FILE_PREFIX')+ datetime.utcnow().strftime('_%Y%m%d_%H%M') +\
            '.xml.gz' if config.get('USE_GZIP') else '.xml'
        s3File = 's3://' + config.get('S3_LOCATION')+ ftpFile
        outFile = s3File if not config.get('IS_LOCAL') else 'test/feeds/' + ftpFile
        if len(data) >  0 and int(config.get('EXPORT_TO_DB')) > 1:
            await dumpToDb(data, config, reload = True)
            del data[:]
        if int(config.get('EXPORT_TO_XML')):
            with smart_open.open(outFile, 'wb') as fout:
                writeToFile(fout, doc)
        print ("Finished", \
            tracker.get('count'), datetime.now() - tracker.get('startTime'))
        result = {
            'processedRecords': tracker.get('count'),
            'finished': tracker.get('complete')
        }
        print ('Result:', result)
        if not config.get('IS_LOCAL'):
            snsNotify(config.get('SNS_NOTIFY_TOPIC'), ctx, result = result)
        return 0
    except Exception as ex:
        return handleException(ex, ctx, \
            notificationTarget = None if config.get('IS_LOCAL')\
                else config.get('SNS_NOTIFY_TOPIC'))

async def accumulate(doc, limit, offset, tracker, config, context):
    data =[]
    print("Aggregating items in db to nest children under parents", offset, limit)
    db = getConnectionAsync(config, domain='product')
    mappings = jsonFileToDict("s3://"+config.get('S3_LOCATION') + config.get('MAPPING_CONF'))
    async for item in db.items.aggregate(\
        getQuery('itemsAggregation', limit = limit, offset = offset), allowDiskUse = True)\
            .batch_size(int(config.get('CURSOR_BATCH_SIZE'))):
        out = {}
        persist = {'skus':{}}
        tracker['count'] +=1
        #print("Processing product: #", tracker.get('count'), datetime.now() - tracker.get('startTime'))
        skulist = []
        childCount = 0
        for sku in item.get('skus'):
            # TODO Figure out moving this part in the aggregation itself
            if sku.get('parent') is None:
                # skus is dict instead of list for faster traversing later
                out = {**sku, 'skus':{}}
            else:
                if len(sku.get('listPrice')) > 0:
                    childCount  += 1
                    out.get('skus')[sku.get('code')] = sku
                    skulist.append(sku.get('code'))
            # Get sales price from offers and merge it to sku dict
        #print ("Merging inventory and offers now", tracker.get('count'), datetime.now() - tracker.get('startTime'))
        # TODO make this truly async
        if int(config.get('GET_FOREIGN_DATA')):
            await getForeignData(out.get('skus'), persist, skulist, config)
        if int(config.get('FILTER_NOSKU')) > 0  and len(out.get('skus')) < 1:
            continue
        if int(config.get('EXPORT_TO_DB')):
            persist['_id'] = out.get('_id')
            persist['code'] = out.get('code')
            bson = dump(persist, binary=True)
            data.append(compressGz(bson))
        if int(config.get('EXPORT_TO_XML')):
            parseAndLoadItem(out, doc.getroot(), {'mappings' : mappings, 'stage':config.get('SERVERLESS_STAGE')})
        if not config.get('IS_LOCAL') and context.get_remaining_time_in_millis() < int(config.get('TIMEOUT_MARGIN')):
            print('Stopping as lambda timeout is approaching. Processed parent products ', tracker.get('count'))
            tracker['complete'] = False
            break
    return data

async def getForeignData(skus, persist, skulist, config):
    tasks = []
    invTask = asyncio.create_task(mergeBaseLnInv(skus, persist, skulist, config))
    tasks.append(invTask)
    offersTask = asyncio.create_task(mergeSalePrice(skus, skulist,config ))
    tasks.append(offersTask)
    return await asyncio.gather(*tasks)

async def mergeSalePrice(skus, skulist, config):
    #print("Start Offers aggregation at ", datetime.now().isoformat())
    if len(skulist) < 1:
        return
    db = getConnectionAsync(config, domain='offer')
    async for offer in db.offers.aggregate(\
        getQuery('offersAggregation', kwargs = {'skulist':skulist, 'dDate':datetime.utcnow()})):
        prices = {}
        offerPrice = {}
        for sp in offer.get('offers'):
            price = {
                'kind' : 'clearance' if sp.get('kind') in ['709','809'] else 'sale',
                'channel':sp.get('channel'),
                'currency':sp.get('price').get('currency'),
                'amount':sp.get('price').get('amount'),
            }
            if offerPrice.get(price.get('channel')) is None:
                offerPrice[price.get('channel')] = price
        # TODO Use Callback
        prices['offerPrice'] = [value for key, value in offerPrice.items()]
        if skus.get(offer.get('itemId')) is not None:
            skus.get(offer.get('itemId')).update(prices)
    #print ("Offers Done ", datetime.now().isoformat())

async def mergeBaseLnInv(skus, persist, skulist, config):
    #print("Start Inventory call at ", datetime.now().isoformat())
    # Proxying to EB's legacy inventory service for now.
    return await mergeBaseLnInvEbImpl(skus, persist, skulist, config)
    #print ("Inventory Done ", datetime.now().isoformat())

# This implementation uses EB's legacy inventory SOAP service.
async def getBaseLnInvEbImpl(skulist, config):
    doc = createElement('Envelope',\
        attributes = [('xmlns','http://schemas.xmlsoap.org/soap/envelope/')], getDocument = True)
    wrapper = createElement('GetBaseLnInv', \
        attributes = [('xmlns', 'http://EB/eCommerce/Services/InventoryService/')],\
            parent = createElement('Body', \
                parent = doc.getroot()))
    items = createElement('SKUs', parent = wrapper)
    for sku in skulist:
        createElement('SKU', text=sku, parent = createElement('ItemSku', parent = items))
    body = printDocument(doc, get = True)
    headers = {
      'Content-Type': 'text/xml;charset=UTF-8',
      'soapAction': 'http://EB/eCommerce/Services/InventoryService/GetBaseLnInv'
    }
    r = await httpPost(config.get('INVENTORY_URL'), session = httpSession, headers = headers, body = body)
    return incrementalParse([r.get('content')])

async def mergeBaseLnInvEbImpl(skus, persist, skulist, config):
    if len(skulist) < 1:
        return
    processedSkus = set()
    response = await getBaseLnInvEbImpl(skulist, config)
    # TODO working of find/findall with namespaces
    for res in response.iter("{*}GetBaseLnInvResult"):
        result = res
    for item in result.iter('{*}ItemBaseLnInv'):
        availCount = 0
        availDict = {}
        sku = item.find("SKU", namespaces = item.nsmap).text
        hasPo = item.find("HasPO", namespaces = item.nsmap).text
        for stock in item.findall("./InvByCountryList/InvByCountry", namespaces = item.nsmap):
            country = stock.find("CountryCode", namespaces = stock.nsmap).text
            avail = int(stock.find("AvailQty", namespaces = stock.nsmap).text)
            if country in ['US', 'CA']:
                if avail > 0 : 
                    availCount += 1
                    availDict["availability%s" % country] = "In Stock"
                elif avail < 1 and hasPo == 'True':
                    availCount += 1
                    availDict["availability%s" % country] = "Back Order"
                else:
                    availDict["availability%s" % country] = "Out Of Stock"
            # TODO replace with callback
        processedSkus.add(sku)
        if int(config.get('FILTER_BY_INVENTORY')) > 0:
            if availCount > 0:
                persist.get('skus')[sku] = availDict
                skus.get(sku).update(availDict)
            else:
                del skus[sku]
        else:
            persist.get('skus')[sku] = availDict
            skus.get(sku).update(availDict)
    if int(config.get('FILTER_BY_INVENTORY')) > 0:
        for sku in (set(skulist) - processedSkus):
            del skus[sku]               

async def dumpToDb(out, config, reload = False):
    """
    If reload == True expect compressed payload, and empty db
    """
    upserts = []
    count = 0
    try:
        db = getConnectionAsync(config, domain='search')
        if reload:
            print ("Emptying db")
            db.bloomreach.drop()
        for elem in out:
            product = None
            if reload:
                product = loads(decompressGz(elem, binary = True), binary = True)
            else:
                product = loads(elem, binary = True)
            upserts.append(UpdateOne({'_id':product['_id']},{'$set':product}, upsert=True))
            count += 1
            del product, elem
            if count == int(config.get('DBDUMP_BATCH_SIZE')):
                resp = await db.bloomreach.bulk_write(upserts)
                print (resp.bulk_api_result)
                del upserts[:]
                count = 0
        if count > 0:
            resp = await db.bloomreach.bulk_write(upserts)
            del upserts[:]
            print (resp.bulk_api_result)
    except BulkWriteError as bwe:
        print (count, bwe.details)
    except Exception as ex:
        print (count, ex)

def pushToDestination(fp, config, event, context):
    ctx = {
        'file':fp,
        'lambda': {
            'function' : 'eddiebauer-feed-bloomreach-push' \
                if context.function_name == 'Fake' else context.function_name,
            'requestId': context.aws_request_id
        }
    }
    try:
        writeToFtp(smart_open.open(fp, 'rb'),\
            config.get('FTP_HOST'), user = config.get('FTP_USER'), password = config.get('FTP_PASSWORD'))
    except Exception as ex:
        return handleException(ex, ctx, \
            notificationTarget = None if config.get('IS_LOCAL')\
                else config.get('SNS_NOTIFY_TOPIC'))

async def processDelta(config, event, context):
    ctx = {
        'lambda': {
            'function' : 'ydv-feed-develop-bloomreach-out-daily' \
                if context.function_name == 'Fake' else context.function_name,
            'requestId': context.aws_request_id
        }
    }
    try:
        tracker = {
            'startTime' : datetime.now(),
            'complete' : True,
            'count' : 0,
            'changeCount' : 0
        }
        limit = int(config.get('LIMIT'))
        offset = int(config.get('OFFSET'))
        if event.get('eventName') == config.get('MANUAL_TEST_EVENT'):
            limit = event.get('query').get('limit')
            offset = event.get('query').get('offset')
        global httpSession
        async with ClientSession() as session:
            httpSession = session
            data = await accumulateDelta(limit, offset, tracker, config, context)
            if len(data.get('fileData')):
                outFile = config.get('OUT_FILE_PREFIX')+ datetime.utcnow().strftime('_%Y%m%d_%H%M.tsv')
                with smart_open.open('s3://' + config.get('S3_LOCATION')+ outFile, mode='w') as fo:
                    tsvw = csv.writer(fo, delimiter='\t')
                    tsvw.writerow(['product_id', 'sku_id', 'availability_us', 'availability_ca'])
                    for row in data.get('fileData'):
                        tsvw.writerow(row)
            if len(data.get('dbData')):
                await dumpToDb(data.get('dbData'), config, reload = False)
            print (tracker['count'], datetime.now() - tracker['startTime'])
            result = {
                'processedProducts': tracker.get('count'),
                'changedSkus' : tracker.get('changeCount'),
                'finished': tracker.get('complete')
            }
            if not config.get('IS_LOCAL'):
                snsNotify(config.get('SNS_NOTIFY_TOPIC'), ctx, result = result)
            return 0
    except Exception as ex:
        return handleException(ex, ctx, \
            notificationTarget = None if config.get('IS_LOCAL')\
                else config.get('SNS_NOTIFY_TOPIC'))

async def accumulateDelta(limit, offset, tracker, config, context):
    print  ("In accumulate delta function")
    db = getConnectionAsync(config, domain='search')
    dbOut = []
    fileOut = []
    async for product in db.bloomreach.find().limit(limit)\
        .batch_size(int(config.get('CURSOR_BATCH_SIZE'))):
        skulist = list(product.get('skus').keys())
        tracker['count'] += 1
        #print ('Processing product %s' % tracker['count'])
        inventory = await getBaseLnInvEbImpl(skulist, config)
        for res in inventory.iter("{*}GetBaseLnInvResult"):
            result = res
        for item in result.iter('{*}ItemBaseLnInv'):
            availCount = 0
            availDict = {}
            hasChanged = False
            sku = item.find("SKU", namespaces = item.nsmap).text
            hasPo = item.find("HasPO", namespaces = item.nsmap).text
            out = [product.get('code'), sku, 'NC','NC']
            for stock in item.findall("./InvByCountryList/InvByCountry", namespaces = item.nsmap):
                country = stock.find("CountryCode", namespaces = stock.nsmap).text
                avail = int(stock.find("AvailQty", namespaces = stock.nsmap).text)
                if country in ['US', 'CA']:
                    if avail > 0 : 
                        availCount += 1
                        availDict["availability%s" % country] = "In Stock"
                    elif avail < 1 and hasPo == 'True':
                        availCount += 1
                        availDict["availability%s" % country] = "Back Order"
                    else:
                        availDict["availability%s" % country] = "Out Of Stock"
            for key, value in availDict.items():
                oldValue = product.get('skus').get(sku).get(key)
                if not oldValue == value:
                    hasChanged = True
                if key == "availabilityUS":
                    out[2] = value
                elif key == "availabilityCA":
                    out[3] = value
            if hasChanged:
                tracker['changeCount'] += 1
                fileOut.append(out)
                product.get('skus').get(sku).update(availDict)
                dbOut.append(dump(product, binary=True))
    return {'fileData': fileOut, 'dbData':dbOut}