import re
from lxml import etree

def parseAndLoadItem(itemNode, root, config):
    global mapping, stage
    mapping = config.get('mappings')
    stage = config.get('stage')
    parentElement = etree.SubElement(root, 'product')
    parentFlags = parseNode(parentElement, itemNode, kind='parent')
    variantsElement = etree.SubElement(parentElement, 'variants')
    defaultLargeImage = None
    setFieldValue(parentElement, 'largeImage', defaultLargeImage)
    for sku, variant in itemNode.get('skus').items():
        derived = parseNode(etree.SubElement(variantsElement, 'sku'), variant, kind='sku')
        largeImage = None
        for key, value in derived.items():
            if key == 'largeImage':
                largeImage = value
            else:
                # If any sku has is_active or is_display True, mark the same for parent as well
                if not parentFlags.get(key) and value == True:
                    # using parentFlages dict for reference as searches through xml element are costlier,
                    # so keeping those searches at bay as far as possible
                    parentFlags[key] = value
                    setFieldValue(parentElement, key, value, upsert=True)
        if derived.get('inStock'):
            if not derived.get('isDefaultSku'):
                if defaultLargeImage is None and largeImage is not None:
                    defaultLargeImage = largeImage
            else:
                if largeImage is not None:
                    defaultLargeImage = largeImage
    setFieldValue(parentElement, 'largeImage', defaultLargeImage, upsert = True)

def parseNode(xmlElement, itemNode, kind='sku'):
    derived = {}
    hasListPriceFor = [] # Should be empty for a parent node
    hasOfferPriceFor = [] #  ""
    for key, value in itemNode.items():
        if key in ['_id', '__v', 'isActive', 'createdAt', 'updatedAt', 'code',\
        'description', 'vendors', 'files', 'videos' ]\
        or (kind == 'parent' and key in ['variants', 'parent', 'skus', 'startDate', 'endDate', 'isSkuIdentical'])\
        or (kind == 'sku' and key in ['title', 'group', 'parent']):
            continue
        elif key == 'attributes':
            derived = parseAttributes(xmlElement, value, kind = kind)
        elif key == 'variants':
            parseAttributes(xmlElement, value, kind = 'variants')
        elif key == 'listPrice':
            hasListPriceFor = parsePrice(xmlElement, value)
        elif key == 'offerPrice':
            parsePrice(xmlElement, value)
        elif key == 'group':
            parseGroup(xmlElement, value)
        elif key == 'gtin':
            setFieldValue(xmlElement, value.get('kind'), value.get('value'))
        elif key == 'images':
            parseImages(xmlElement, value)
        elif key == 'channel':
            if kind == 'parent':
                for channel in value:
                    url = getProductUrl(itemNode.get('sku'), channel)
                    setFieldValue(xmlElement, url.get('key'), url.get('value'))
            # elif kind == 'sku': #TODO handle this differently
        else:
            if key.startswith('availability') and value in ['In Stock', 'Back Order']:
                derived['inStock'] = True
            setFieldValue(xmlElement, key, value, types=[kind])
    for channel in hasListPriceFor:
        channel = 'CA' if channel == 'ca' else 'US'
        setFieldValue(xmlElement, 'publishTo%s' %channel, True, upsert=True)
    if derived is not None:
        return derived

def getRoot(name, attributes = None):
    return createRootElement(name)

def setFieldValue(parentXmlElement, name, value, types = [], upsert = False):
    # TODO Waiting for AWS to add Python 3.8 runtime support
    # So that inline assignment expressions can help cleanup a lot of boilerplate
    # if upsert and (element:=parentXmlElement.find(getXmlTagName(name))) is not None:
    if upsert:
        element = parentXmlElement.find(getXmlTagName(name))
        if element is not None:
            element.text = etree.CDATA(parseValue(value))
        else:
            etree.SubElement(parentXmlElement, getXmlTagName(name, types))\
            .text = etree.CDATA(parseValue(value))
    else:
        etree.SubElement(parentXmlElement, getXmlTagName(name, types))\
        .text = etree.CDATA(parseValue(value))

def parseAttributes(element, attributes, kind = 'sku'):
    flags = {} #Provide these values upstream to the caller
    ref = dict(getMandatoryAttributes(kind))
    defaultCanvasImage = None
    for attribute in attributes:
        if attribute.get('name') in ['CategoryImage', 'CategorySmall', 'Swatch', 'SearchImage', 'DefaultCanvasImage']:
            value = 'https://eddiebauer.scene7.com/is/image/EddieBauer/%s' %(attribute.get('value').split('?')[0])
            setFieldValue(element, attribute.get('name'), value)
            if attribute.get('name') == 'DefaultCanvasImage':
                defaultCanvasImage = value
        elif kind in  ['parent', 'sku'] and ref.get(attribute.get('name')) is not None:
            setFieldValue(element, attribute.get('name'), attribute.get('value'))
            del ref[attribute.get('name')]
            if attribute.get('name') in ['isActive', 'isDisplay', 'isDefaultSku']:
                flags[attribute.get('name')] = attribute.get('value')
        elif kind not in  ['parent', 'sku']:
            # the third kind, 'variants' get processed here
            setFieldValue(element, attribute.get('name'), attribute.get('value'))
    # Now set the default values for attributes that we didn't get from db
    for key, value in ref.items():
        setFieldValue(element, key, value)
    del ref
    if defaultCanvasImage is not None:
        flags['largeImage'] = defaultCanvasImage
    return flags

def parseGroup(xmlElement, groups):
    count = 0
    for group in groups:
        count += 1
        crumbs = []
        crumbsId = []
        while len(group.get('parents')) > 0:
            parent = group.get('parents').pop()
            if parent.get('code') in ['cat10001', 'cat10002', 'cat40001', 'cat40002']:
                continue
            crumbs.append(parent.get('name'))
            crumbsId.append(parent.get('code'))
        if group.get('code') in ['cat10001', 'cat10002', 'cat40001', 'cat40002']:
            continue
        crumbs.append(group.get('name'))
        crumbsId.append(group.get('code'))
        setFieldValue(xmlElement, 'crumbs_'+str(count), crumbs)
        setFieldValue(xmlElement, 'crumbsId_'+str(count), crumbsId)

def parsePrice(skuElement, priceList):
    hasPriceFor = []
    if priceList is None:
        return
    for price in priceList:
        if not isinstance(price, dict):
            return
        kind = price.get('kind') if price.get('kind') is not None else 'list'
        if price.get('amount') > 0:
            hasPriceFor.append(price.get('channel'))
        setFieldValue(skuElement, 'amount', price.get('amount'), types = [kind, price.get('channel')])
    return hasPriceFor

def parseValue(value):
    if value == None:
        return ''
    if isinstance(value, (list)):
        return '|'.join(value)
    if isinstance(value, bool) and value == True:
        return 'T'
    elif isinstance(value, bool) and value == False:
        return 'F'
    elif isinstance(value, (int, float)):
        return str(value)
    elif isinstance(value, str):
        return value
    else:
        return str(value)

def parseImages(element, images):
    lCount = 0
    aCount = 0
    for image in images:
        url = image.get('url').split('?')[0]
        """if url[-2] == 'C':
            if lCount < 1:
                lCount += 1
                setFieldValue(element, 'large_image',\
                    'https://eddiebauer.scene7.com/is/image/EddieBauer/%s' %url)
            else:
                aCount += 1
                setFieldValue(element, 'alt_image_%d' %aCount,\
                    'https://eddiebauer.scene7.com/is/image/EddieBauer/%s' %url)
        else:"""
        aCount += 1
        setFieldValue(element, 'alt_image_%d' %aCount,\
            'https://eddiebauer.scene7.com/is/image/EddieBauer/%s' %url)
    #if lCount == 0:
        # Did not find large image, so add a placeholder element for now.
        #setFieldValue(element, 'large_image','')

def getProductUrl(productId, channel):
    data = {}
    prefix = 'dev01'
    if stage == 'staging':
        prefix = 'test'
    if stage == 'production':
        prefix = 'www'
    if channel in ['com', 'ca']:
        data['key'] = 'url_%s' % channel
        data['value'] = 'https://%s.eddiebauer.%s/p/%s' % (prefix, channel, productId)
    return data

def getMandatoryAttributes(kind):
    if mapping is not None:
        return mapping.get('mandatoryAttributes').get(kind)

def getXmlTagName (name, types=['default']):
    nameMap = mapping.get('tagNameMap')
    newName =  nameMap.get(name)
    if newName is not None:
        if isinstance(newName, dict):
            for t in types:
                newName = newName.get(t) if newName.get(t) is not None else newName.get('default')
        return newName
    else:
        return camelToSnake(name)

# TODO move to designated lib
def camelToSnake(string) :
    firstCapRe = re.compile('(.)([A-Z][a-z]+)')
    allCapRe = re.compile('([a-z0-9])([A-Z])')
    s1 = firstCapRe.sub(r'\1_\2', string)
    return allCapRe.sub(r'\1_\2', s1).lower()
