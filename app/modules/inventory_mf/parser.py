#Parser file to process the input txt file.
import datetime

def prepare_inventory(parse_data):
    invent_data = {}
    try:
        today = datetime.date.today()
        data = parse(parse_data)
        print ("Preparing inventory", data)
        invent_data['sku_id'] = fetch_sku(data)
        invent_data['created_at'] = today.strftime("%m/%d/%y")
        invent_data['updated_date'] = today.strftime("%m/%d/%y")
        invent_data['stock_level'] = data['stocklevel']
        invent_data['po1_date'] = data['poDate1']
        invent_data['po1_qty'] = data['poQty1']
        invent_data['po2_date'] = data['poDate2']
        invent_data['po2_qty'] = data['poQty2']
        invent_data['po3_date'] = data['poDate3']
        invent_data['po3_qty'] = data['poQty3']
        invent_data['taxcode'] = data['taxCode']
    except Exception as e:
        print(str(e))
    return invent_data


def parse(input):
    data = {}
    try:
        data['dept'] = input[1:4]
        data['style'] = input[4:8]
        data['color'] = input[8:11]
        data['sizeName'] = input[11:17]
        data['sizeCode'] = input[17:21]
        data['qtyOnHand'] = int(input[25:32])
        data['qtyOesCom'] = int(input[33:40])
        data['qtyOesSub'] = int(input[41:48])
        data['qtyBO'] = int(input[50:56])
        data['qtySold'] = int(input[57:64])
        data['taxCode'] = input[138:143]
        poDate1 = input[143:151]
        poQty1 = input[152:159]
        data['POMAP1'] = {'Date': parseDate(poDate1), 'poQty1': poQty1}
        poDate2 = input[159:167]
        poQty2 = input[168:175]
        data['POMAP2'] = {'Date': parseDate(poDate2), 'poQty2': poQty2}
        poDate3 = input[175:183]
        poQty3 = input[184:191]
        data['POMAP3'] = {'Date': parseDate(poDate3), 'poQty3': poQty3}
        data['stocklevel'] = findstock(data)
        data['poDate1'] = parseDate(poDate1)
        data['poDate2'] = parseDate(poDate2)
        data['poDate3'] = parseDate(poDate3)
        data['poQty1'] = poQty1
        data['poQty2'] = poQty2
        data['poQty3'] = poQty3
    except Exception as e:
        print(str(e))
    return data


# Help function for finding the stocklevel
def findstock(data):
    print ("Finding Stock", data)
    stock = 0
    try:
        stock = data['qtyOnHand'] - (data['qtyOesCom'] + data['qtyOesSub'] + data['qtyBO'] + data['qtySold'])
        if stock < 0:
            stock = 1
    except Exception as e:
        print(str(e), "exception at finding stock")
    return stock



#help to fetch_sku for sku_id as per input
def fetch_sku(data):
    from .process import get_product_sku
    if data:
        return get_product_sku(data)

def parseDate(dateString) :
    if dateString == '99999999':
        return ''
    else:
        dateList = list(dateString)
        dateList[0] = '2'
        return "".join(dateList)

