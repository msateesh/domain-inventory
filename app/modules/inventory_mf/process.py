# Imports from python distribution
import time
import sys
import csv
import asyncio
import datetime
from smart_open import smart_open
from requests import Session

# Intra-project imports
from .parser import prepare_inventory
from ...lib.httprequest import post
from ...lib.s3utils import readFromS3, lsS3, rmS3Files, copyS3File
from ...lib.errorutils import handleException
from ...lib.mongoutils import getConnection, getConnectionAsync


def accumulate(fp, config):
    header = {}
    try:
        global db, db_2
        db = getConnection(config, domain='inventory')
        db_2 = getConnection(config, domain='product')
        col_name = 'inventory_eb'
        with smart_open(fp,'r', encoding='utf-8-sig') as fh:
            count = 0
            for line in fh:
                line = line.strip()
                if len(line.strip()) == 207:
                    data = prepare_inventory(line)
                    print(data)
                    if not check(data, col_name):
                        insert(data,col_name)
                    else:
                         update(data, col_name)
    except Exception as e:
        print(str(e))


def parseAndPopulate(fp, config, event, context):
    ctx = {
        'file':fp,
        'lambda': {
            'function' : 'eddiebauer-feed-develop-inventory_eb' \
                if context.function_name == 'Fake' else context.function_name,
            'requestId': context.aws_request_id
        }
    }
    try:
        batchSize = int(config.get('batchSize'))
        pauseAfter = int(config.get('pauseAfterBatch')) * batchSize
        tracker = {'count':0, 'total':0, 'startTime' : datetime.datetime.now()}
        data = {}
        rows = []
        s3ls = lsS3(config.get('S3_BUCKET'), prefix = config.get('PREFIX'))
        if not s3ls or len(s3ls) is not 1:
            print ('Insufficient data', s3ls)
            return 0
        else:
            print ("Data files OK, proceed to processing")
            for fileData in s3ls:
                fp = "s3://%s/%s" %(config.get('S3_BUCKET'), fileData.get('Key'))
                if not int(config.get('SKIP_LOAD')):
                    accumulate(fp, config)
                [fileData.pop(k) for k in list(fileData.keys()) if k not in ['Key', 'VersionId', 'Bucket']]
                fileData['Bucket'] = config.get('S3_BUCKET')
                print ("Sending file to archive location", fileData)
                copyS3File(fileData, fileData.get('Bucket'), \
                    "%s/%s" %(config.get('ARCHIVE_PREFIX'), fileData.get('Key').split('/')[-1]))
                # s3 delete_object will reject Bucket, so removing it
                del fileData['Bucket']
        print ("Deleting files from drop location", s3ls)
        rmS3Files(config.get('S3_BUCKET'), s3ls)
        return 0
    except Exception as ex:
        return handleException(ex, ctx, notificationTarget = config.get('SNS_NOTIFY_TOPIC'))


# Function to insert data into mongo db
def insert(data, col_name):
    try:    
        print("Enter into insert method")
        db.inventory_eb_test.insert_one(data)
        print('\nInserted data successfully\n')
    
    except Exception as e:
        print(str(e))

# function to read records from mongo db
def read(col_name):
    try:
        inventrys = db.inventory_eb_test.find()
        print('\n All data from Database \n')
        for inventry in inventrys:
            print(inventry)

    except Exception as e:
        print(str(e))
        

# Function to update record to mongo db
def update(data, col_name):
    try:
        db.inventory_eb_test.update_one(
            {'sku_id':data['sku_id']},
            {
                "$set": data
            }
        )
        print("\nRecords updated successfully\n")    
    except Exception as e:
        print(str(e))


# Function to Check record in mongo db
def check(data, col_name):
    try:
        res = db.inventory_eb_test.find_one({'sku_id':data['sku_id']})
        print(res)
        if res != None:
            return True
        else:
            return False 
    except Exception as e:
        print(str(e))

# Function to delete record from mongo db
def delete(data, col_name):
    try:
        db.inventory_eb_test.delete_many(data)
        print('\nDeletion successful\n') 
    except Exception as e:
        print(str(e))

# Function to find the product from product_eddiebaur items collection
def get_product_sku(data):
    try:
        item = db_2.items.find_one({'attributes':{'$elemMatch':{'$and':[{'name':'mf_sizeName', 'value':data['sizeName']},{'name':'dept', 'value':data['dept']},{'name':'ColorId', 'value':data['color']},{'name':'style', 'value':data['style']}]}}})
    except Exception as e:
        print(str(e))
    if item != None:
        print("Item Found", item)
        return item.sku
    else:
        return data['dept'] + data['color'] + data['style'] + data['sizeName']