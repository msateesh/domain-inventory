# Imports from python distribution
import time, datetime
import sys, traceback
from re import finditer, compile

# External libraries
from lxml import etree
import requests

# Intra-project imports
from .parser import parseProduct
from ...lib.httprequest import post
from ...lib.s3utils import readFromS3
from ...lib.jsonutils import dump
from ...lib.awsutils import invokeAsyncLambda, snsNotify
from ...lib.errorutils import handleException

def parseAndPopulate(fp, config, event, context):
    ctx = {
        'file':fp,
        'lambda': {
            'function' : 'ydv-feed-develop-products' \
                if context.function_name == 'Fake' else context.function_name,
            'requestId': context.aws_request_id
        }
    }
    try :
        print ('Info: ', ctx)
        skipLines = 0
        processedProducts = 0
        if event.get('eventName') == config.get('DELEGATE_EVENT_NAME'):
            skipLines = event.get('data').get('skipLines')
            processedProducts = event.get('data').get('processedProducts')
        starttime = datetime.datetime.now()
        result = accumulate(readFromS3(fp), config, skipLines, processedProducts, context)
        print ('Result:',result)
        if result.get('accessError'):
            return 5000
        if not result.get('finished'):
            delegateEvent = {}
            if event.get('eventName') == config.get('DELEGATE_EVENT_NAME'):
                delegateEvent = event
                delegateEvent.get('data')['skipLines'] = result.get('skipLines')
                delegateEvent.get('data')['processedProducts'] = result.get('processedProducts')
                delegateEvent.get('eventSource').append("aws:lambda:"+context.aws_request_id)
            else:
                result['s3FileUrl'] = fp
                delegateEvent = {
                    'eventName' : config.get('DELEGATE_EVENT_NAME'),
                    'eventSource': ["aws:lambda:"+context.aws_request_id],
                    'data': result
                }
            print("Delegate Event : ", delegateEvent)
            invokeAsyncLambda(context.function_name, dump(delegateEvent))
        print ('Elapsed time ', datetime.datetime.now()-starttime)
        snsNotify(config.get('SNS_NOTIFY_TOPIC'), ctx, result = result)
        return 0

    except Exception as ex:
        return handleException(ex, ctx, notificationTarget = config.get('SNS_NOTIFY_TOPIC'))

def accumulate(iterable, config, skipLines, processedProducts, context):
    tracker = {
        'lineCount' : 0,
        'count' : 0,
        'totalproducts' : processedProducts
    } 
    products = []
    elem ={}
    s = requests.Session()
    for line in iterable:
        tracker['lineCount']  += 1
        if tracker['lineCount'] <= skipLines:
            continue
        line = line.decode('utf8')
        if "<Product>" in line :
            elem['start'] = line
            elem['body'] =''
        elif "</Product>" in line :
            elem['end'] = line
            tracker['count'] += 1
            tracker['totalproducts'] += 1

            feed = elem.get('start') + elem.get('body') + elem.get('end')

            rex =  compile(config.get('INVALID_XML_REGEX'))
            for match in rex.finditer(feed):
                strlist = list(feed)
                strlist[match.start()] = ''
                feed = "".join(strlist)
            elemT = {}
            try:
                elemT =  etree.fromstring(feed)
                products.append(parseProduct(elemT))
            except etree.XMLSyntaxError as e:
                raise Exception(("[etree.XMLSyntaxError] Bad XML Syntax For product at: \n"+ str(tracker)))
            if sys.getsizeof(dump(products)) >= int(config.get('PAYLOAD_SIZE')):
                leftOver =  products.pop()
                if not sendPost(config, elemT, products, s, tracker, leftOver):
                    return {
                        'accessError':True
                    }
                print ('Time left ',context.get_remaining_time_in_millis())
                if not config.get('IS_LOCAL') and context.get_remaining_time_in_millis() < int(config.get('TIMEOUT_MARGIN')):
                    print("Exiting as lambda timeout is approaching. Processing done till line number", tracker.get('lineCount'))
                    return {
                        'skipLines': tracker.get('lineCount'),
                        'processedProducts': tracker.get('totalproducts'),
                        'finished': False
                    }
            elem = {}
            continue

        elif tracker.get('lineCount') <= 2 or "</Products>" in line:
            continue
        else:
            elem['body'] += line.strip()
    if sys.getsizeof(dump(products)) < int(config.get('PAYLOAD_SIZE')):
        if not sendPost(config, None, products, s, tracker, None):
            return {
                'accessError':True
            }

    return {
        'skipLines': tracker.get('lineCount'),
        'processedProducts': tracker.get('totalproducts'),
        'finished': True
    }

def sendPost(config, elem, products, s, tracker, leftOver):
    status = 0
    attempt = 0
    while status not in range(200, 499) :
        attempt +=1
        print ("Posting ",  tracker.get('totalproducts'), tracker.get('count'), attempt, tracker.get('lineCount'))
        response = post(config, products, s)
        status = response.status_code
        if status in [401, 403]:
            return False
        print ('Response :: ', status, response.content)
        if status in range(200, 499) or attempt > int(config.get('RETRY_POST_ITEM_LIMIT')): #400 coz we cannot repair a bad request midwway
            if attempt > int(config.get('RETRY_POST_ITEM_LIMIT')):
                print ("Retry limit reached, skipping this batch")
            tracker['count'] = 0
            del products [:]
            if leftOver is not None:
                products.append(leftOver)
                leftOver =  None
            if elem is not None:
                cleanup(elem)
            attempt = 0
            break
        else:
            print ("Reattempt Posting ",  tracker.get('totalproducts'), tracker.get('count'))
    return True

def cleanup (elem) :
    # cleanup
    elem.clear()
    # delete previous siblings (records)
    while elem.getprevious() is not None:
        #print 'hasPrev'
        del elem.getparent()[0]
