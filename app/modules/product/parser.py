from lxml import etree
from html import unescape

def parseProduct(productNode):
    product = {}
    attributes = []
    channels = set()
    keywords = []
    images = []
    for child in productNode :
        if child.tag == 'ProductId':
            product['code'] = child.text.strip()
            product['sku'] = child.text.strip()
        elif child.tag == 'effort': # Ignore this
            continue
        elif child.tag == 'newProductName':
            product['title'] = unescape(child.text).strip()
        elif child.tag == 'DisplayName':
            addAttribute(child, attributes, newName='displayName')
            if not product.get('title'):
                # [Temp] Title Fallback to DisplayName
                product['title'] = unescape(child.text).strip()
        elif child.tag == 'newProductDesc':
            product['description'] = unescape(child.text).strip()
        # [Temp] Description fallback to pdpTabCopy1
        elif child.tag == 'pdpTabCopy1':
            if not product.get('description'):
                product['description'] = unescape(child.text).strip()
            else:
                addAttribute(child, attributes)
        elif child.tag == 'summaryProductCopy':
            addAttribute(child, attributes, 'summary')
        elif child.tag == 'startDate':
            product['startDate'] = child.text.strip()
        elif child.tag == 'endDate':
            product['endDate'] = child.text.strip()
        elif child.tag == 'canvasImages':
            addImage(child, images)
        elif child.tag == 'reviews':
            parseReviews(child, attributes)
        elif child.text and child.tag == 'siteAwareAssets':
            parseChannel(unescape(child.text).strip(), channels)
        elif child.tag == 'PdpfeaturesList':
            pdpFeaturesList = []
            for grandchild in child.iterfind('./newPdpfeaturesList'):
                pdpFeaturesList.append(unescape(grandchild.text).strip())
            addAttributeKvp('pdpFeaturesList', pdpFeaturesList, attributes)
        elif child.tag == 'PdpMaterialList':
            pdpMaterialList = []
            for grandchild in child.iterfind('./newPdpMaterialList'):
                pdpMaterialList.append(unescape(grandchild.text).strip())
            addAttributeKvp('pdpMaterialList', pdpMaterialList, attributes)
        elif child.tag == 'keywords':
            keywords.append(unescape(child.text).strip())
        elif child.tag == 'categories':
            product['group'] = parseGroups(child.findall('./merchantCategories'))
        elif child.tag == 'Skus':
            product['Skus'] = parseItems(child.findall('./Sku'), product, channels)
        else :
            addAttribute(child, attributes, None)
    addAttributeKvp('keywords', keywords, attributes)
    product['attributes'] = attributes
    product['images'] = images
    product['channel'] = list(channels)
    return product



def parseGroups(categories):
    groups = []
    categoryStrings = []
    for category in categories:
        codes = category.find('merchantCategoryIds').text.split('_')
        names = category.find('merchantCategory').text.split('|')

        incoming = {
            'codes': codes,
            'names': names,
        }
        categoryStrings.append(incoming)

        # Mimic the group topology as a flattened tree of (code,name) tuples
        flatTree = []
        for code, name in zip(incoming.get('codes'), incoming.get('names')):
            flatTree.append((code,name))

        # use only the leaf of the tree to construct the group
        leaf = flatTree.pop()
        group = dict(zip(('code', 'name'), leaf))

        # remaining nodes of the tree become parents, bottoms-up
        group['parents'] = []
        while len(flatTree) > 0:
            parent = flatTree.pop()
            if not parent == leaf:
                group['parents'].append({'code':parent[0], 'name':parent[1]})

        groups.append(group)

    return groups

def parseItems(itemsNode, parent, channelsOfParent):
    items = []
    for itemNode in itemsNode :        
        item = {}
        gtin = {}
        variants = []
        attributes = []
        channels= set()
        for child in itemNode :
            if child.tag == 'SkuId':
                item['code'] = child.text.strip()
                item['sku'] = child.text.strip()
            elif child.tag == 'effort': #Ignore this
                continue
            elif child.text and child.tag == 'siteAwareAssets':
                parseChannel(unescape(child.text).strip(), channels)
            elif child.tag == 'startDate':
                item['startDate'] = child.text.strip()
            elif child.tag == 'endDate':
                item['endDate'] = child.text.strip()
            elif child.tag == 'price':
                setPrice(child, item)
            elif child.tag == 'upc':
                gtin['kind'] = 'UPC'
                gtin['value'] = child.text.strip()
                item['gtin'] = gtin
            elif (child.tag == 'colorName') or (child.tag == 'sizeName') or (child.tag == 'styleName') :
                addAttribute(child, variants, None) #Adding to variants array here
            else :
                addAttribute(child, attributes, None) #Add everything else to attributes

        channelsOfParent.update(channels)
        item['channel'] = list(channels)

        item['variants'] = variants
        item['attributes'] = attributes

        if item.get('startDate') is None :
            item['startDate'] = parent.get('startDate')
        if item.get('endDate') is None:
            item['endDate'] = parent.get('endDate')
        items.append(item)
    return items

def parseChannel(value, channels):
    if value == 'EBSite':
        channels.add('com')
    elif value == 'EBSiteCA':
        channels.add('ca')

def parseReviews(node, attributes) :
    for child in node:
        value = child.text
        if child.tag == 'rating':
            value = float(child.text)
        if child.tag == 'numberOfReviews':
            value = int(child.text)
        addAttributeKvp(child.tag, value, attributes)

def addAttributeKvp(key, value, attributes) :
    attributes.append(
        {
            'name' : key,
            'value' : value
        }
    )

def addAttribute(node, attributes, newName = None) :
    attrib = {}
    attrib['name'] = node.tag
    if node.text:
        attrib['value'] = lookForBoolean(unescape(node.text))
    if newName not in [None, '']:
        attrib['name'] = newName
    attributes.append(attrib)

def addImage(node, images) :
    image = {}
    image['meta'] = node.tag
    image['url'] = unescape(node.text)
    images.append(image)

def lookForBoolean(value):
    if value is not None:
        if value.lower() == 'true':
            return True
        elif value.lower() == 'false':
            return False
    return value

def setPrice(priceNode, item) :
    listPrices = []
    for child in priceNode:
        if item.get('listPrice') is not None:
            # Temporary check to stop the wrongfully added canada list price
            # from leaking into the US list price
            break
        if child.text is not None and child.tag == "regularPrice_US" :
            listPrice = {}
            listPrice['currency'] = 'USD'
            listPrice['amount'] = float(unescape(child.text))
            listPrice['channel'] = 'com'
            listPrices.append(listPrice)
        elif child.text is not None and child.tag == "regularPrice_CA" :
            listPrice = {}
            listPrice['currency'] = 'CAD'
            listPrice['amount'] = float(unescape(child.text))
            listPrice['channel'] = 'ca'
            listPrices.append(listPrice)
    item['listPrice'] = listPrices
