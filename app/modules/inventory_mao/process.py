# Imports from python distribution
import time
import sys
import csv
import datetime

from smart_open import smart_open
from requests import Session

# Intra-project imports
from .parser import parse
from ...lib.httprequest import post
from ...lib.s3utils import readFromS3, lsS3, rmS3Files, copyS3File
from ...lib.errorutils import handleException

def accumulate(fp, config, skuData):
    header = {}
    with smart_open(fp,'r', encoding='utf-8-sig') as fh:
        count = 0
        for line in fh:
            line = line.strip()
            if line:
                count += 1
            if count == 1: # First row
                row = next(csv.reader([line]))
                if row:
                    header = row
            else:
                row = next(csv.reader([line]))
                if row:
                    newData = parse(row, header)
                    sku = newData.get('code')
                    oldData = skuData.get(sku)
                    if oldData:
                        # already exists from first file, just adding country store data   
                        del newData['Quantity']['DistributionCenters']
                        oldData.get('Quantity').update(newData.get('Quantity'))
                    else:
                        skuData[sku] = newData

def parseAndPopulate(fp, config, event, context):
    ctx = {
        'file':fp,
        'lambda': {
            'function' : 'eddiebauer-feed-develop-inventory-mao' \
                if context.function_name == 'Fake' else context.function_name,
            'requestId': context.aws_request_id
        }
    }
    try:
        batchSize = int(config.get('batchSize'))
        pauseAfter = int(config.get('pauseAfterBatch')) * batchSize
        tracker = {'count':0, 'total':0, 'startTime' : datetime.datetime.now()}
        data = {}
        rows = []
        s3ls = lsS3(config.get('S3_BUCKET'), prefix = config.get('PREFIX'))
        if not s3ls or len(s3ls) is not 2:
            print ('Insufficient data')
            return 0
        else:
            print ("Data files OK, proceed to processing")
            for fileData in s3ls:
                fp = "s3://%s/%s" %(config.get('S3_BUCKET'), fileData.get('Key'))
                if not int(config.get('SKIP_LOAD')):
                    accumulate(fp, config, data)
                [fileData.pop(k) for k in list(fileData.keys()) if k not in ['Key', 'VersionId', 'Bucket']]
                fileData['Bucket'] = config.get('S3_BUCKET')
                print ("Sending file to archive location", fileData)
                copyS3File(fileData, fileData.get('Bucket'), \
                    "%s/%s" %(config.get('ARCHIVE_PREFIX'), fileData.get('Key').split('/')[-1]))
                # s3 delete_object will reject Bucket, so removing it
                del fileData['Bucket']
            # Create HTTP Session and make bulkinserts
            s = Session()
            for key, value in data.items():
                rows.append(value)
                tracker['count'] += 1
                tracker['total'] += 1
                if tracker.get('count') == batchSize:
                    sendPost(config, rows, s, tracker)
            if tracker.get('count') in range(1, batchSize):
                sendPost(config, rows, s, tracker)
        print ("Deleting files from drop location", s3ls)
        rmS3Files(config.get('S3_BUCKET'), s3ls)
        return 0
    except Exception as ex:
        return handleException(ex, ctx, notificationTarget = config.get('SNS_NOTIFY_TOPIC'))

def sendPost(config, rows, s, tracker):
    status = 0
    attempt = 0
    while status is not 200 :
        attempt +=1
        print ("Posting ",  tracker.get('total'), tracker.get('count'), attempt, sys.getsizeof(rows))
        response = post(config, rows, s)
        status = response.status_code
        print ('Response :: ', status, response.content)
        if status == 200:
            tracker['count'] = 0
            del rows [:]
            attempt = 0
            break
        else:
            print ("Reattempt Posting ",  tracker.get('total'), tracker.get('count'))
