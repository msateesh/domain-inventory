from datetime import datetime
def parse(row, header) :
    poBuffer = {}
    Quantity = {}
    data = {}
    country = 'US'
    for key, value in zip(header, row):
        key = key.strip()
        if key == 'CountryCode':
            country = value
        if key == 'ItemId':
            data['code'] = value
        elif key.startswith('PO') and key.endswith('Date'):
            poNumber = key[2:-4]
            if value and poBuffer.get('po-'+poNumber) is None:
                poBuffer['po-'+poNumber] = {
                    'PoNumber': int(poNumber)
                }
            if value:
                poBuffer.get('po-'+poNumber)['PoNextAvailabilityDate'] = parseDate(value)  
        elif key.startswith('PO') and key.endswith('QTY'):
            poNumber = key[2:-3]
            if value and poBuffer.get('po-'+poNumber) is None:
                poBuffer['po-'+poNumber] = {
                    'PoNumber': int(poNumber)
                }
            if value:
                poBuffer.get('po-'+poNumber)['PoQuantity'] = int(value)
        elif key == 'DCQty':
            Quantity['DistributionCenters'] = int(value)
        elif key == 'StoreQty':
            Quantity[country + '-Stores'] = int(value)

    data['PoDetail'] = list(poBuffer.values())
    data['Quantity'] = Quantity
    return data

def parseDate(dateString) :
    fields  = dateString.split('/')
    return datetime(int(fields[2]), int(fields[0]), int(fields[1])).isoformat()




    