import sys, os, traceback
from pysftp import Connection, CnOpts, helpers
from io import BytesIO
from datetime import date
from app.lib.s3utils import processResult, checkIfKeyExists, writeToS3


def handler(event, context):
    try :
        print ("Event:", event)
        host = os.environ.get('SFTP_HOST')
        cnopts = CnOpts()
        hostkeys = None
        if cnopts.hostkeys.lookup(host) == None:
            print("New host - will accept any host key")
            hostkeys = cnopts.hostkeys
            cnopts.hostkeys = None
        with Connection(host, username=os.environ.get('SFTP_USER'), password=os.environ.get('SFTP_PASSWORD') , cnopts=cnopts)as sftp:
            today = date.today()
            dayOffset = 0 if event.get('eventName') != os.environ.get('MANUAL_TEST_EVENT') \
                else int(event.get('data').get('dayOffset'))
            datePrefix = str(today.year) + str(today.month) + str(today.day + dayOffset)
            print ("Today\'s date prefix", datePrefix)
            with sftp.cd(os.environ.get('SFTP_DIR')):   
                fileAttrs = sftp.listdir_attr()
                fileAttrs.sort(key = lambda f: f.st_mtime)
                s3Location = 's3://' + os.environ.get('S3_BUCKET') + os.environ.get('S3_PREFIX')
                chosenOne = None
                doTransfer = True
                if event.get('eventName') == os.environ.get('MANUAL_TEST_EVENT')\
                    and event.get('data') is not None\
                        and not event.get('data').get('transfer'):
                        doTransfer = False
                while len(fileAttrs) > 0:
                    lastFile = fileAttrs.pop()
                    # Check in archive location once we have that setup
                    matchesCriteria = lastFile.filename.startswith('InventoryFeed_'+ datePrefix) and lastFile.filename.endswith('.txt')
                    print (matchesCriteria,"Matching",  lastFile,"Last File")
                    if not matchesCriteria:
                        continue
                    keyExists = checkIfKeyExists(s3Location + lastFile.filename)
                    print (s3Location + lastFile.filename, 'Exists:', keyExists)
                    if not keyExists:
                        print("NewFile:",lastFile)
                        chosenOne =  lastFile
                        if doTransfer:
                            with BytesIO() as feedFile:
                                sftp.getfo(chosenOne.filename, feedFile)
                                print (sys.getsizeof(feedFile))
                                writeToS3(s3Location + chosenOne.filename, feedFile,\
                                    bool(os.environ.get('STREAM_TO_S3')),int(os.environ.get("CHUNK_SIZE")))
                if chosenOne is None:
                    print ("Returning as there is no new file to transfer")
        return processResult(0)

    except Exception as ex:
        print ("Error in process::",ex)
        exc_info = sys.exc_info()
        traceback.print_exception(*exc_info)
        del exc_info
        return processResult(5000) #Return specific to exception occurred 


