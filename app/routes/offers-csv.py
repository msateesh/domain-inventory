import os
from app.lib.s3utils import handleS3Put
from app.lib.testutils import handleLocalFile
from app.modules.offer_csv.process import parseAndPopulate

def handler(event, context) :
    if event.get('eventName') == "local":
        return handleLocalFile(event, context, os.environ, parseAndPopulate)
    else:
        return handleS3Put(event, context, os.environ, parseAndPopulate)