import os
from app.lib.s3utils import handleS3Put
from app.lib.testutils import handleTestEvent
from app.lib.eventutils import handleGenericEvent
from app.modules.bloomreach_out.process import processDelta

def handler(event, context) :
    if event.get('eventName') == os.environ.get('MANUAL_TEST_EVENT'):
        return handleTestEvent(event, context, os.environ, processDelta, aio = True)
    else:
        return handleGenericEvent(event, context, os.environ, processDelta, aio = True)