import os
from app.lib.s3utils import handleS3Put
from app.lib.testutils import handleLocalFile
from app.modules.bloomreach_out.process import pushToDestination

def handler(event, context) :
    if event.get('eventName') == "local":
        return handleLocalFile(event, context, os.environ, pushToDestination)
    else :
        return handleS3Put(event, context, os.environ, pushToDestination)
