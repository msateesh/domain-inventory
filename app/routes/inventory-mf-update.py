import os
from app.lib.s3utils import handleS3Put
from app.lib.testutils import handleLocalFile
from app.modules.inventory_mf.process import parseAndPopulate

def handler(event, context) :
    return handleLocalFile(event, context, os.environ, parseAndPopulate)
