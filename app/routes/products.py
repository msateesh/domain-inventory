import os
from app.lib.s3utils import handleS3Put, handleDelegatedS3Event
from app.lib.testutils import handleLocalFile
from app.modules.product.process import parseAndPopulate

def handler(event, context) :
    if event.get('eventName') == os.environ.get('DELEGATE_EVENT_NAME') :
        return handleDelegatedS3Event(event, context, os.environ, parseAndPopulate)
    elif event.get('eventName') == "local":
        return handleLocalFile(event, context, os.environ, parseAndPopulate)
    else :
        return handleS3Put(event, context, os.environ, parseAndPopulate)
