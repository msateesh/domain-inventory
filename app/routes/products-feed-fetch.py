import sys, os, traceback
from pysftp import Connection, CnOpts, helpers
from smart_open import smart_open
from io import BytesIO
from app.lib.s3utils import processResult, checkIfKeyExists, writeToS3, writeGzToS3FromZip

def handler(event, context ):
    try :
        print ("Event:", event)
        host = os.environ.get('SFTP_HOST')
        cnopts = CnOpts()
        hostkeys = None
        if cnopts.hostkeys.lookup(host) == None:
            print("New host - will accept any host key")
            hostkeys = cnopts.hostkeys
            cnopts.hostkeys = None
        with Connection(host, username=os.environ.get('SFTP_USER'), password=os.environ.get('SFTP_PASSWORD') , cnopts=cnopts)as sftp:
            #if hostkeys != None:
                #print("Connected to new host, caching its hostkey")
               # hostkeys.add(host, sftp.remote_server_key.get_name(), sftp.remote_server_key)
                #hostkeys.save(helpers.known_hosts())
            with sftp.cd(os.environ.get('SFTP_DIR')):   
                fileAttrs = sftp.listdir_attr()
                fileAttrs.sort(key = lambda f: f.st_mtime)
                for fileAttr in fileAttrs:
                    print(fileAttr.filename, fileAttr)

                s3Location = 's3://' + os.environ.get('S3_BUCKET') + os.environ.get('S3_PREFIX')
                chosenOne = None
                while len(fileAttrs) > 0:
                    lastFile = fileAttrs.pop()
                    # Check in archive location once we have that setup
                    keyExists = checkIfKeyExists(s3Location + lastFile.filename)
                    print (s3Location + lastFile.filename, 'Exists:', keyExists)
                    if not keyExists:
                        print("NewFile:",lastFile)
                        chosenOne =  lastFile
                    else:
                        print("One existing file found, not going beyond")
                        break
                print("ChosenOne:",chosenOne)
                if event.get('eventName') == os.environ.get('MANUAL_TEST_EVENT')\
                    and event.get('data') is not None\
                        and not event.get('data').get('transfer'):
                        return processResult(0)
                if chosenOne is None:
                    print ("Returning as there is no new file to transfer")
                    return processResult(0)

                with BytesIO() as feedFile:
                    sftp.getfo(chosenOne.filename, feedFile)
                    print (sys.getsizeof(feedFile))
                    if chosenOne.filename.endswith('.xml'):
                        # Stream to s3, as this can be a huge file
                        writeToS3(s3Location + chosenOne.filename, feedFile,\
                            bool(os.environ.get('STREAM_TO_S3')),int(os.environ.get("CHUNK_SIZE")))

                    elif chosenOne.filename.endswith('.zip'):
                        # Create a dummy file for tracking
                        writeToS3(s3Location + chosenOne.filename, BytesIO(), False, 0)
                        # write the actual content, as .gz
                        writeGzToS3FromZip(s3Location, feedFile)

        return processResult(0)

    except Exception as ex:
        print ("Error in process::",ex)
        exc_info = sys.exc_info()
        traceback.print_exception(*exc_info)
        del exc_info
        return processResult(5000) #Return specific to exception occurred 


