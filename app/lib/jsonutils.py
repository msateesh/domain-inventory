import json, bson
import smart_open


def jsonFileToDict(fp):
    with smart_open.open(fp) as f:
        out = json.load(f)
        return out


def dump(data, pretty=False, binary = False):
    if binary:
        return bson.BSON.encode(data)
    if pretty:
        return json.dumps(data, sort_keys=True, indent=2, separators=(",", ": "))
    return json.dumps(data)

def loads(data, binary = False):
    if binary:
        return bson.BSON.decode(data)
    return json.loads(data)

