import sys
from .jsonutils import dump
# To be deprecated in favour of async impl using aiohttp
def post(config, body, session):
    body = dump(body)
    print ("Body Size in bytes ", sys.getsizeof(body))
    endpoint = config.get('DOMAIN_API_HOST') + config.get('BATCH_INSERT_ENDPOINT')
    if config.get('IS_LOCAL'):
        endpoint = config.get('API_LOCALHOST') + config.get('BATCH_INSERT_ENDPOINT')

    with session :
        session.headers.update({'x-api-key': config.get('DOMAIN_API_KEY')})
        r = session.post(endpoint, data=body)
        return r

async def httpPost(url, session = None, headers = None, body = None, sem = None, timeout = 0.10000):
    if sem is None:
        async with session.post(url, data = body, headers = headers) as response:
            return  {
                'content':await response.read(),
                'status':response.status
            }
    else:
        async with sem, session.post(url, data = body, headers = headers) as response:
            return  {
                'semaphore':sem._value,
                'content':await response.read(),
                'status':response.status
            }
