import boto3
from .jsonutils import dump

def invokeAsyncLambda (lambdaName, event):
    client = boto3.client('lambda')
    client.invoke(
        FunctionName=lambdaName,
        InvocationType='Event',
        Payload=event
    )

def snsNotify(topic, ctx, result=None, error=None):
    sns = boto3.client('sns')
    fnNameParts = ctx.get('lambda').get('function').split('-')
    subject = fnNameParts[1] + ' | ' + fnNameParts[3] + ' | ' + fnNameParts[2] + ' | ' + fnNameParts[0]
    message = "\nInvocation Details :\n" + dump(ctx, pretty=True)
    if error is not None:
        subject =  '[Error] ' + subject
        message = message + "\n\nError Stacktrace :\n" + error
    elif result is not None:
        subject =  '[Success] ' + subject
        message = message + "\n\nSuccess Summary :\n" + dump(result, pretty=True)
    sns.publish(
        TopicArn = topic,
        Message = message,
        Subject = subject
    )