from smart_open import smart_open
import boto3
from zipfile import ZipFile

def handleDelegatedS3Event(event, context, env, processor):
    s3Fp = event.get('data').get('s3FileUrl')
    print ("Event:",event)
    result = processor(s3Fp, env, event, context)
    return processResult(result)

def handleS3Put(event, context, env, processor):
    s3Records = event.get('Records')
    if len(s3Records) > 0 :
        for s3Record in s3Records:
            if s3Record['eventSource'] == 'aws:s3' and s3Record['s3'] is not None:
                print ("Event: ", s3Record['eventName'])
                key = s3Record['s3'].get('object').get('key')
                bucketName = s3Record['s3'].get('bucket').get('name')
                # Assusming that object key and bucket name are true to the configured event
                s3Fp  = 's3://'+bucketName+'/'+key
                result = processor(s3Fp, env, event, context)
                return processResult(result)
            else:
                return {
                    'statusCode': 400,
                    'body': 'Not an s3 put event'
                }

def checkIfKeyExists(fp):
    try:
        with smart_open(fp, 'rb') as fin:
            return True
    except ValueError as err:
        print (err)
        return False
    except Exception as ex:
        print (ex)

def readFromS3(fp, mode = 'rb'):
    return smart_open.open(fp, mode)

def lsS3(bucket, prefix = None):
    print (bucket, prefix)
    s3 = boto3.client('s3')
    obj = s3.list_objects_v2(Bucket = bucket, Prefix = prefix)
    return obj.get('Contents')
    #return bucket.objects.all()

def rmS3Files(bucket, files):
    s3 = boto3.client('s3')
    response = s3.delete_objects(
    Bucket=bucket,
    Delete={
        'Objects': files
    })

def copyS3File(srcPath, destBucket, destkey):
    s3 = boto3.resource('s3')
    s3.Object(destBucket,destkey).copy_from(CopySource=srcPath)

# chunkSize > 0 mandatory if doStream is true
# Call from a try block
def writeToS3(fp, inputFileObj, doStream, chunkSize):
    with smart_open(fp, 'wb') as fout:
        print ("Now writing file to s3, using stream=", doStream)
        if doStream:
            eof = inputFileObj.seek(0,2)
            inputFileObj.seek(0)
            while True :
                chunk = inputFileObj.read1(chunkSize)
                if chunk == b'' and inputFileObj.tell() == eof:
                    break
                fout.write(chunk)
                del chunk
        else:
            fout.write(inputFileObj.getvalue())

# Assumes that the zip contains only one file.
# To be changed later if we have more than one file in the zip.
def writeGzToS3FromZip(s3Location, fin):
    with ZipFile(fin) as zipped:
        zfp = zipped.namelist().pop()
        fp = s3Location + zfp + '.gz'
        with zipped.open(zfp) as f:
            with smart_open(fp ,'wb') as fout:
                for line in f:
                    fout.write(line)

def processResult(result):
    if result == 0:
        return {
            'statusCode': 200,
            'body': {
                'message': 'Records Inserted Successfully'
            }
        }
    elif result == 5000:
        return {
            'statusCode': 500,
            'body': {
                'message': 'Something went wrong'
            }
        }
