import sys, traceback
from .awsutils import snsNotify


def handleException(ex, ctx, notificationTarget=None):
    try:
        exc_info = sys.exc_info()
        exc = traceback.format_exception(*exc_info)
        print("Error: ", ctx)
        error = ""
        for line in exc:
            error = error + line
        print(error)
        if notificationTarget is not None:
            snsNotify(notificationTarget, ctx, error=error)
        del exc_info
    except:
        pass
    return 5000  # Return specific to exception occurred

