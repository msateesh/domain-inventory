from pymongo import MongoClient, ReadPreference
from urllib.parse import quote_plus
from motor.motor_asyncio import AsyncIOMotorClient

def getConnectionAsync(config, domain = 'product'):
    return AsyncIOMotorClient(config.get('MONGODB_URL'))\
    .get_database(domain + '_eddiebauer', read_preference=ReadPreference.SECONDARY_PREFERRED)

def getConnection(config, domain = 'product'): 
    return MongoClient(config.get('MONGODB_URL'))\
    .get_database(domain + '_eddiebauer', read_preference=ReadPreference.SECONDARY_PREFERRED)
