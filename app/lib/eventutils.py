import asyncio

def handleGenericEvent(event, context, env, processor, aio = False):
    print ('Event: ',event)
    if aio:
        loop = asyncio.get_event_loop()
        task = asyncio.ensure_future(processor(env, event, context))
        return processResult(loop.run_until_complete(task))
    else:
        result = processor(env, event, context)
        return processResult(result)

def processResult(result):
    if result == 0:
        return {
            'statusCode': 200,
            'body': {
                'message': 'Records Inserted Successfully'
            }
        }
    elif result == 5000:
        return {
            'statusCode': 500,
            'body': {
                'message': 'Something went wrong'
            }
        }