from io import BytesIO
import gzip
from ftplib import FTP

def compressGz(input, obj = False):
    """
    read the given string, encode it in utf-8,
    compress the data and return it as a byte array.
    """
    bio = BytesIO()
    if isinstance(input, str):
        input = input.encode("utf-8")
    bio.write(input)
    bio.seek(0)
    stream = BytesIO()
    compressor = gzip.GzipFile(fileobj=stream, mode='w')
    while True:  # until EOF
        chunk = bio.read(8192)
        if not chunk:  # EOF?
            compressor.close()
            if obj:
                stream.seek(0)
                return stream
            else:
                return stream.getvalue()
        compressor.write(chunk)

def decompressGz(inputBytes, binary = False):
    """
    read the given byte string, 
    decompress the data and return it as a string
    """
    in_ = BytesIO()
    in_.write(inputBytes)
    in_.seek(0)
    with gzip.GzipFile(fileobj=in_, mode='rb') as fo:
        gunzipped = fo.read()
        if binary:
            return gunzipped
        return gunzipped.decode()

def writeToFtp(fp, host, user = None, password = None, outFile = None):
    """
    Supports gzip compression as well.
    """
    if outFile == None:
        # Use the src file name as dest file name, get it from file obj
        outFile = getSrcFilePath(fp).split('/')[-1]
    with FTP(host, user = user, passwd = password) as ftp:
        print ('Connected to ftp')
        if outFile.endswith('.gz'):
            ftp.storbinary('STOR '+outFile, compressGz(fp.read(), obj = True))
        else:
            ftp.storbinary('STOR '+outFile, fp)


def getSrcFilePath(fp):
    """
    Extract file path from file object.
    Currently supports gzip among compressed formats.
    """
    if isinstance(fp, gzip.GzipFile):
        if 'name' in dir(fp.fileobj) :
            #GzipFile from disk
            return fp.fileobj.name
        if '_object' in dir(fp.fileobj):
            #GzipFile from s3
            return 's3://%s/%s' %(fp.fileobj._object.bucket_name, fp.fileobj._object.key)
    else:
        if 'name' in dir(fp) :
            #Simple File from disk
            return fp.name
        if '_object' in dir(fp):
            #Simple GzipFile from s3
            return 's3://%s/%s' %(fp._object.bucket_name, fp._object.key)