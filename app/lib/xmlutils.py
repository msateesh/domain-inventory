from lxml.etree import XMLParser, ElementTree, Element, SubElement, CDATA, tostring
def incrementalParse(iterator):
    # Incrementally create the document root element
    parser = XMLParser()
    for line in iterator:
        parser.feed(line)
    return parser.close()

def createDocumentRoot(name, attributes = []):
    # To be deprecated for createElement(getDocument=True)
    root = Element(name)
    setAttributes(root, attributes)
    doc = ElementTree(root)
    return doc

def createElement(name, attributes = [], text = None, cdata = False, parent = None, getDocument = False):
    element = SubElement(parent, name) if parent is not None else Element(name)
    setAttributes(element, attributes)
    if text is not None:
        element.text = CDATA(text) if cdata else text
    return ElementTree(element) if getDocument else element

def setAttributes(element, attributes):
    # attributes is a list of tuples
    # handle IndexError upstream
    for attribute in attributes:
        element.attrib[attribute[0]] = attribute[1]

def writeToFile(fp, doc, encoding = 'utf-8', declareXml = True, pretty = True):
    # To be deprecated in favour of printDocument(doc,fp="path to file")
    doc.write(fp, encoding=encoding, xml_declaration=declareXml, pretty_print=pretty)

"""
Usage:
- printDocument(doc)                    # Simply print to sysio
- printDocument(doc, get=True)          # Returns the byte string which can then be used by the caller
- printDocument(doc, fp='path/to/file') # Prints to the given file, ignores other kwargs
"""
def printDocument(doc, fp = None, encoding = 'utf-8', declareXml = True, pretty = True, get = False):
    if fp is not None:
        if str(type(doc)) == "<class 'lxml.etree._Element'>":
            doc = ElementTree(doc)
        doc.write(fp, encoding=encoding, xml_declaration=declareXml, pretty_print=pretty)
    else:
        ret = tostring(doc, encoding=encoding, xml_declaration=declareXml, pretty_print=pretty).decode(encoding)
        if get:
            return ret
        else:
            print(ret)
    return
