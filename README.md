##### Note

Please go through [this confluence wiki](https://yottadv.atlassian.net/wiki/spaces/all/pages/99942602/How+to+handle+Python3+quirks+on+a+Mac) once. Lot's of useful quirks there.

##### Development, testing, Local invocation

This project uses a `.env` file to populate the environment variables on first run. Values of some of the variables in the file have been omitted from the commit as they are api keys. You may get the keys from a jenkins build or from the developer.

Since this project uses `lxml` for parsing feeds, and `lxml` relies on some native binaries, cross-platform packaging is achived using `docker`. So if your intention is just to test locally, please take care of the following:

In serverless.yml, around line 90:

```
+    dockerizePip: true  #set this to false for local testing
```

Now, proceed with the invocation:

```
npm install
pipenv install
npm start -- -f products -p test/events/product.json

#similarly  -f offers and -f inventory-mao
```

Sample events are located in `test/events`

Replace bucket->name and object->key with the values your desired test feed file on s3

For contents of event.json, refer [this aws doc](https://docs.aws.amazon.com/lambda/latest/dg/eventsources.html#eventsources-s3-put).


##### Packaging and deployment on AWS (from a Mac):

1. Make sure `dockerizePip` is set to `true` in `serverless.yml`
2. Install docker:
`brew cask install docker`
3. Create a docker hub account, if you don't have already, and make note of your username (not email).
4. Login to docker from terminal using username (not email)
`docker login`
5. Make sure you have proper aws credentials on your machine.
6. You can test the packaging using `npm run pack`
7. `npm run deploy -- --dockerize` packages an uploads the function(s) to aws.
8. `npm run deploy -- -p` skips packaging, and can use the packaging from step 6.
9. Other options can be supplied after `npm run deploy -- `. For example:

  `npm run deploy -- --dockerize --clientId eddiebauer --stage develop --aws-profile eb`

   clientId is used in serverless.yml to generate the service name etc, and stage also used by serverless. `dockerize` switches the use of docker for build.
   If you have multiple aws profiles you can chose one using aws-profile option



##### Packaging and deployment on AWS (from Amazon Linux / RHEL/ Centos)
Might also work for debian distros.

Amazon Linux is prefered, since that is what lambda functions run on.

- Requirements:
1. python3.7 and pip
2. pipenv (can be installed using `pip install pipenv`)
3. npm (and hence node)
4. Docker should not be required here.

- Preparing for deployment:

1. Make sure all the required environment variables are there in the environment. If deploying manually, there is a file called `.env` in the root of the repo which gets loaded into the python environment. Or you can find other way as well.

2. `npm install`

- Deployment

3. `npm run deploy -- --clientId eddiebauer --stage staging --aws-profile eb`

   clientId (defaults to `ydv`) is used in serverless.yml to generate the service name etc, and stage also used by serverless.
   If you have multiple aws profiles you can chose one using aws-profile option.
   Set the `stage` (defaults to develop) accordingly as well.
   





